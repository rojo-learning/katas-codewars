# Codewars Ruby Katas
Ruby exercises from the [Codewars](http://www.codewars.com/) platform to help master the language features
using the TDD approach.

This solutions use the RSpec test framework to guide the solution instead of the
test framework provided in the Codewars website, so the tests can be run on and
environment with a typical Ruby installation.

## Contents
- **Refactored Greeting**: Object oriented refactoring of a simple greeter.
- **Broken Greetings**: Upcoming.
- **Multiply**: Upcoming.

---
This repository may contain samples of code from the _Codewars_ site. These are
included under _fair use_. The code of the solutions to the exercises in this
repository should be considered public domain.
