
class Person
  attr_reader :name

  def initialize(name)
    @name = name
  end

  def greet(other_name)
    "Hello #{other_name}, my name is #{@name}"
  end
end

# Struct version
#
# Person = Struct.new :name do
#   def initialize(name)
#     self.name = name
#   end
#
#   def greet(other_name)
#     "Hello #{other_name}, my name is #{self.name}"
#   end
# end
