
# Refactored Greeting
# The following code could use a bit of object oriented artistry. While its a
# simple method and works just fine as it is, in a larger system its best to
# organize methods into classes/objects. Refactor the following code so that it
# belongs to a Person class/object. Each Person instance will have a greet
# method. The Person instance should be instantiated with a name so that it no
# longer has to be passed into each greet method call.
#
# TODO: This method needs to be called multiple times for the same person
# (myName). It would be nice if we didnt have to always pass in myName every
# time we needed to great someone.
#
# def greet(myName, yourName)
#   "Hello #{yourName}, my name is #{myName}"
# end
#
# Here is how the final refactored code would be used:
#
# joe = Person.new('Joe')
# joe.greet('Kate') # should return 'Hello Kate, my name is Joe'
# joe.name # should == 'Joe'

require_relative 'refactored_greeting'

describe Person do
  let(:name) { 'Rojo'                   }
  subject    { described_class.new name }

  it 'name is the given name' do
    expect(subject.name).to eq(name)
  end

  describe '#greet' do
    let(:greeted) { 'Vic' }

    specify 'the greet contains the greeter\'s name' do
      expect(subject.greet greeted).to match /#{name}/
    end

    specify 'the greet contains the greeted name' do
      expect(subject.greet greeted).to match /#{greeted}/
    end

    specify 'the greet satisfies the greeting format' do
      format = "Hello #{greeted}, my name is #{name}"
      expect(subject.greet greeted).to eq(format)
    end
  end
end
